
public class main {
	static int minProductSubset(int array[]) {
		if (array.length == 1)
			return array[0];

		int negativeMaxNumber = Integer.MIN_VALUE;
		int positiveMinNumber = Integer.MAX_VALUE;
		int count_neg = 0, count_zero = 0;
		int product = 1;

		for (int i = 0; i < array.length; i++) {

			// count number of zero
			if (array[i] == 0) {
				count_zero++;
				continue;
			}

			// count the negative numbers
			// and find the max negative number
			if (array[i] < 0) {
				count_neg++;
				negativeMaxNumber = Math.max(negativeMaxNumber, array[i]);
			}

			// find the minimum positive number
			if (array[i] > 0 && array[i] < positiveMinNumber)
				positiveMinNumber = array[i];

			product *= array[i];
		}

		// There are all zeroes
		// or zero is exist but negative number is not
		if (count_zero == array.length || (count_neg == 0 && count_zero > 0))
			return 0;

		// All positive
		if (count_neg == 0)
			return positiveMinNumber;

		// There are even number of negative numbers and no zeros
		if (count_neg % 2 == 0 && count_neg != 0) {
			// The result is the product of all except the largest valued negative number.
			product = product / negativeMaxNumber;
		}

		return product;
	}

	public static void main(String[] args) {

		int array[] = { 1, 5, 2, 4, 10 };

		System.out.println(minProductSubset(array));
	}

}
